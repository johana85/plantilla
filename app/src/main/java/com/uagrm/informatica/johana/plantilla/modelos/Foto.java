package com.uagrm.informatica.johana.plantilla.modelos;

import java.util.Date;

public class Foto {
    private String url;
    private String ur_thumbail;
    private String title;
    private int like;
    private String fecha_publicacion;
    private int user_id;

    public Foto(String url, String ur_thumbail, String title, int like, String fecha_publicacion,
                int user_id) {
        this.url = url;
        this.ur_thumbail = ur_thumbail;
        this.title = title;
        this.like = like;
        this.fecha_publicacion = fecha_publicacion;
        this.user_id = user_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUr_thumbail() {
        return ur_thumbail;
    }

    public void setUr_thumbail(String ur_thumbail) {
        this.ur_thumbail = ur_thumbail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public String getFecha_publicacion() {
        return fecha_publicacion;
    }

    public void setFecha_publicacion(String fecha_publicacion) {
        this.fecha_publicacion = fecha_publicacion;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
