package com.uagrm.informatica.johana.plantilla;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.uagrm.informatica.johana.plantilla.almacenamiento.SharedPref;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

       // SharedPref sharedPref=  new SharedPref(this);

        Handler handler= new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               if(!SharedPref.isLogin())
                {
                    gotoLoginActivity();
                }else
                {
                    gotoMainActivity();
                }
            }
        },3000);
    }

    private void gotoLoginActivity() {
        Intent intent= new Intent(this,LoginActivity.class);
        startActivity(intent);
        finish();

    }

    private void gotoMainActivity() {
        Intent intent= new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();

    }
}
