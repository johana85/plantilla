package com.uagrm.informatica.johana.plantilla.almacenamiento;

import android.content.Context;
import android.content.SharedPreferences;


public class SharedPref {
    private static SharedPreferences preferences;
    private static Context context;
    private static final String PREF = "PLANTILLA";
    private static final String NOT_FOUND = "N";
    private static final String USER_EMAIL = "USER_EMAIL";
    private static boolean isLogin;

    public SharedPref(Context context) {
        this.context = context;
        isLogin = false;
    }

    public static void write(String key, String value) {
        preferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        preferences.edit().putString(key, value).commit();
    }

    public static String read(String key) {
        preferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return preferences.getString(key, NOT_FOUND);
    }

    public static boolean isLogin() {
        return isLogin;
    }

    public static void logout()
    {
        isLogin=false;
    }

}
