package com.uagrm.informatica.johana.plantilla.adaptadores;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uagrm.informatica.johana.plantilla.R;

public class MyViewHolder extends RecyclerView.ViewHolder {

    private ImageView image;
    private TextView title;
    private ImageView icon1;
    private ImageView icon2;

    public MyViewHolder(View itemView) {
        super(itemView);
        this.image = (ImageView) itemView.findViewById(R.id.card_view_image);
        this.title =(TextView) itemView.findViewById(R.id.cardview_title);
        this.icon1 = (ImageView) itemView.findViewById(R.id.cardview_icon1);
        this.icon2 = (ImageView) itemView.findViewById(R.id.cardview_icon2);
    }

    public ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public ImageView getIcon1() {
        return icon1;
    }

    public void setIcon1(ImageView icon1) {
        this.icon1 = icon1;
    }

    public ImageView getIcon2() {
        return icon2;
    }

    public void setIcon2(ImageView icon2) {
        this.icon2 = icon2;
    }
}
