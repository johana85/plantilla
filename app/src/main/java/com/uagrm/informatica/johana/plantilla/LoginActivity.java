package com.uagrm.informatica.johana.plantilla;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button btn_ingresar= (Button)findViewById(R.id.btn_ingresar);
        btn_ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email="";
                String password="";
                if(datosValidos(email,password))
                {
                   verificarDatos(email,password);
                }
            }
        });

        TextView reset=(TextView) findViewById(R.id.txt_resetpassword);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarMensaje("reset pass :)");
            }
        });
        TextView registrarse=(TextView) findViewById(R.id.txt_registrarse);
        registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mostrarMensaje("registrarse ... <3");
                gotoRegisterActivity();
            }
        });
    }

    private void mostrarMensaje(String s) {
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }

    private void verificarDatos(String email, String password) {
        gotoMainActivity();
    }

    private void gotoMainActivity() {
        Intent intent= new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    //verificar que se ingrese un email y un password de tamaño minimo
    private boolean datosValidos(String email, String password) {
        return true;
    }
    public void gotoRegisterActivity()
    {
        Intent intent= new Intent(this,RegisterActivity.class);
        startActivity(intent);
        //finish();
    }

}
